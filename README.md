## Project setup

### Clone repo

`git clone https://gitlab.com/exewin/phaser-golf.git`

### Setup

`cd phaser-golf`

`npm install`

`npm start`

localhost:7474

### Build and run production:

`cd phaser-golf`

`npm install`

`npm run build`

`npm install -g serve`

`serve www`

localhost:3000

### Optional

If `npm start` doesn't work, try removing `/node_modules`, use older version of node (14.19.3) with `nvm` and reinstall packages


![Alt text](extras/1.gif)
