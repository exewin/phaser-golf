import Phaser from 'phaser'
import { MainScene } from './MainScene.js'
import { DataScene } from './DataScene.js'

const config = {
  width: 1024,
  height: 768,
  backgroundColor: '#333333',
  type: Phaser.AUTO,
  parent: 'phaser-game',
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 800 },
      debug: false
    }
  },
  scene: [DataScene, MainScene]
}

new Phaser.Game(config)
