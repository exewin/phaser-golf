import Phaser from 'phaser'
import { Background } from "./Background.js"
import { Ball } from "./Ball.js"
import { Flag } from "./Flag.js"
import { GroundTiles } from "./GroundTiles.js"
import { eventsCenter } from "./EventsCenter.js"

export class MainScene extends Phaser.Scene {
  constructor () {
    super("MainScene")
  }

  preload () {
    this.load.image("background", "../assets/background.png")
    this.load.image("object_ball", "../assets/object_ball.png")
    this.load.image("object_dot", "../assets/object_dot.png")
    this.load.image("object_flag_anim_01", "../assets/object_flag_anim01.png")
    this.load.image("object_flag_anim_02", "../assets/object_flag_anim02.png")
    this.load.image("object_flag_stick", "../assets/object_flag_stick.png")
    this.load.image("object_hole", "../assets/object_hole.png")
    this.load.image("tile_ground_down_01", "../assets/tile_ground_down_01.png")
    this.load.image("tile_ground01", "../assets/tile_ground01.png")
    this.load.image("tile_sky_color01", "../assets/tile_sky_color01.png")
  }

  create () {
    new Background(this)
    const groundTiles = new GroundTiles(this)

    let difficulty = 1

    const score = this.add.text(1000, 24, "0",
      { fontSize: "48px", color: "#fa0", rtl: true, fontFamily: "Arial", stroke: "#000", strokeThickness: 2 })

    eventsCenter.on("update-text", updateText, this)
    this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => {
      eventsCenter.off("update-text", this.updateText, this)
    })

    eventsCenter.on("game-over", gameOver, this)
    this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => {
      eventsCenter.off("game-over", this.gameOver, this)
    })

    function holeCheck () {
      eventsCenter.emit("update-score")
      this.scene.restart()
    }

    function updateText (s) {
      score.setText(s)
      difficulty = 1 + s
    }

    eventsCenter.emit("get-score")

    const minHolePosition = 512
    const maxHoleDistance = 400

    const randomHolePosition = Math.floor(minHolePosition + Math.random() * maxHoleDistance)
    const flag = new Flag({ scene: this, randomHolePosition })

    const ballStartPosition = { x: 50, y: 568 }
    const ball = new Ball({ scene: this, ballStartPosition, difficulty })
    this.ball = ball

    const screenCenterX = this.cameras.main.worldView.x + this.cameras.main.width / 2
    const screenCenterY = this.cameras.main.worldView.y + this.cameras.main.height / 2
    const gameOverWindow = this.add.rectangle(9999, 9999, 256, 128, 0x3377dd).setInteractive()
    gameOverWindow.setStrokeStyle(4, 0xffaa00)

    const gameOverText = this.add.text(9999, 9999, "GAME OVER",
      { fontSize: "32px", color: "#fa0", fontFamily: "Arial", stroke: "#000", strokeThickness: 2 }
    ).setOrigin(0.5)

    gameOverWindow.on("pointerup", function (pointer) {
      eventsCenter.emit("zero-score")
      this.scene.restart()
    }, this)

    function gameOver () {
      setTimeout(() => {
        gameOverWindow.setPosition(screenCenterX, screenCenterY)
        gameOverText.setPosition(screenCenterX, screenCenterY)
      }, 100)
    }

    this.physics.add.collider(this.ball.collider, groundTiles.collider)
    this.physics.add.overlap(this.ball.collider, flag.collider, holeCheck, null, this)
  }

  update (time, delta) {
    this.ball.ballData.ballUpdate(time, delta)
  }
}
