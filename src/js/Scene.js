import { Scene as PhaserScene } from 'phaser'

import DraggableCircle from './DraggableCircle'


export default class Scene extends PhaserScene {
  constructor() {
    super()
  }

  preload() {
    //ladowanie asetow
  }

  create() {
    const bezierGraphics = this.add.graphics()

    const drawBezier = () => {
      const bezier = new Phaser.Curves.CubicBezier(...points)
      bezierGraphics
        .clear()
        .lineStyle(3, 0xffffff)
      bezier.draw(bezierGraphics)
    }

    const points = [
      { x: 100, y: 100, color: 0xff0000, text: 'start point' },
      { x: 300, y: 50, color: 0x00ff00, text: 'control point 1' },
      { x: 300, y: 350, color: 0x00ff00, text: 'contol point 2' },
      { x: 500, y: 300, color: 0xff0000, text: 'end point' },
    ].map(({ x, y, color, text }, index) => {
      const textObject = this.add.text(0, index * 20, '')
      const updateText = (x, y) => {
        textObject.text = `${text}: [${x.toFixed(2)}, ${y.toFixed(2)}]`
      }
      updateText(x, y)

      const circle = new DraggableCircle(this, { x, y, color, radius: 10 })
        .on('move', drawBezier)
        .on('move', updateText)

      this.add.existing(circle)
      return circle
    })

    drawBezier()
  }
}