import Phaser from 'phaser'
import { eventsCenter } from "./EventsCenter.js"

export class Ball extends Phaser.GameObjects.GameObject {
  constructor (data) {
    const { scene, ballStartPosition, difficulty } = data
    const ballScale = 0.5
    const ball = scene.physics.add.image(ballStartPosition.x, ballStartPosition.y, "object_ball").setScale(ballScale).refreshBody()

    super(scene, ballStartPosition, difficulty)

    const dotNumber = 11

    const dots = scene.physics.add.staticGroup({
      key: "object_dot",
      repeat: dotNumber,
      setScale: { x: 0.5 },
      setXY: { x: -100, y: 0, stepX: 0 }
    })

    let allowShoot = true
    let shootActive = false
    let shootPower = 0
    let ballLand = false
    let factor = 0

    this.collider = ball

    function throwBall () {
      allowShoot = false
      shootActive = false
      ball.setVelocityY(-shootPower)
      ball.setVelocityX(shootPower)
      shootPower = 0
      hideDots()
    }

    // controls
    scene.input.mouse.disableContextMenu()
    scene.input.on("pointerup", function (pointer) {
      if (pointer.leftButtonReleased() && allowShoot) {
        throwBall()
      }
    })
    scene.input.on("pointerdown", function (pointer) {
      if (pointer.leftButtonDown() && allowShoot) {
        shootActive = true
        if (factor > 950) {
          throwBall()
        }
      }
    })

    function setDotPositions () {
      dots.children.entries.forEach((dot, index) => {
        const xpos = ballStartPosition.x + (factor * (index / dotNumber))
        const curving = 170
        const high = 16
        const mid = Math.round(dotNumber / 2)
        const yfpos = index * high * factor / 200 - (index * index * factor / curving)
        const yspos = Math.abs(index - dotNumber) * high * factor / 200 - Math.abs(index - dotNumber) * Math.abs(index - dotNumber) * factor / curving
        const ypos = index < mid ? yfpos : yspos
        dot.setPosition(xpos, ballStartPosition.y - ypos)
      })
    }

    function ballUpdate (time, delta) {
      if (ball.body.touching.down && !allowShoot && !ballLand) {
        ballLand = true
        eventsCenter.emit("game-over")
      }
      if (shootActive) {
        shootPower += 0.3 * (1 + difficulty / 5) * delta
        factor = (shootPower * shootPower / 402)
        setDotPositions(factor)
        if (factor > 950) {
          throwBall()
        }
      }
      if (ballLand) {
        ball.setVelocityX(0)
      }
    }

    function hideDots () {
      dots.children.entries.forEach((dot) => {
        dot.setPosition(-100, 0)
      })
    }

    this.ballData = { ballUpdate, shootPower }
  }
}
