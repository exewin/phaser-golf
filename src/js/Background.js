import Phaser from 'phaser'
export class Background extends Phaser.GameObjects.GameObject {
    constructor(data){
        super(data)
        this.scene.add.image(256, 384, 'background').setScale(0.5)
        this.scene.add.image(768, 384, 'background').setScale(0.5)
        this.scene.add.image(512, 704, 'tile_ground_down_01').setScale(16,2)
        this.scene.add.image(512, 64, 'tile_sky_color01').setScale(16,2)
    }
}