import Phaser from 'phaser'

export class Flag extends Phaser.GameObjects.GameObject {
  constructor (data) {
    const { scene, randomHolePosition } = data
    scene.anims.create({
      key: "flagAnimation",
      frames: [
        { key: "object_flag_anim_01" },
        { key: "object_flag_anim_02" }
      ],
      frameRate: 3,
      repeat: -1
    })
    const hole = scene.physics.add.staticImage(randomHolePosition, 620, "object_hole")
    scene.add.image(randomHolePosition, 457, "object_flag_stick")
    scene.add.sprite(randomHolePosition + 73, 380, "object_flag_anim_01").play("flagAnimation")
    super(scene, randomHolePosition)
    this.collider = hole
  }
}
