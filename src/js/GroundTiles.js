import Phaser from "phaser"

export class GroundTiles extends Phaser.GameObjects.GameObject {
  constructor (data) {
    super(data)
    this.collider = this.scene.physics.add.staticGroup({
      key: "tile_ground01",
      repeat: 8,
      setXY: { x: 64, y: 648, stepX: 128 }
    })
  }
}
