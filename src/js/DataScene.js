import Phaser from 'phaser'
import { eventsCenter } from './EventsCenter.js'

export class DataScene extends Phaser.Scene {
  constructor () {
    super('data-scene')
  }

  create () {
    this.score = 0

    this.scene.run('MainScene')

    eventsCenter.on('update-score', this.updateCount, this)
    this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => eventsCenter.off('update-score', this.updateCount, this))

    eventsCenter.on('get-score', this.returnScore, this)
    this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => eventsCenter.off('get-score', this.returnScore, this))

    eventsCenter.on('zero-score', this.zeroScore, this)
    this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => eventsCenter.off('zero-score', this.zeroScore, this))
  }

  zeroScore () {
    this.score = 0
    eventsCenter.emit('update-text', this.score)
  }

  updateCount () {
    this.score++
  }

  returnScore () {
    eventsCenter.emit('update-text', this.score)
  }
}
